package cn.edu.zust.lxy.wangke_backend.mapper;

import cn.edu.zust.lxy.wangke_backend.model.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author keboom
 * @date 2021/6/14 20:22
 */
@Mapper
public interface BookMapper {

    @Select("select book_id,book_title,book_author,book_publish,book_profile,book_appointment,book_borrow from book")
    List<Book> getBookList();

    @Select("select book_id from book where book_title = #{book_title}")
    Integer getIdByBookTitle(String book_title);

    @Update("update book set book_appointment = 1 where book_id = #{bookId}")
    void setAppointmented(int bookId);

    @Update("update book set book_borrow = 1 where book_id = #{bookId}")
    void setBorrowed(int bookId);

    @Select("select book_title,book_author,book_publish,book_appointment,book_borrow from book where book_id = #{book_id}")
    Book getBookById(Integer book_id);

    @Select("select book_id,book_title,book_author,book_publish,book_profile,book_appointment,book_borrow from book where book_title like #{name}")
    List<Book> queryBooks(String name);
}
