package cn.edu.zust.lxy.wangke_backend.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author keboom
 * @date 2021/6/15 16:28
 */
@Mapper
public interface AppointmentMapper {

    @Select("select book_id from appointment where user_id = #{userId}")
    List<Integer> getIdByUserId(Integer userId);
}
