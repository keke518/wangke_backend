package cn.edu.zust.lxy.wangke_backend.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author keboom
 * @date 2021/6/14 12:22
 */
@Setter
@Getter
@ToString
public class Book {

    private Integer book_id;
    private String book_title;
    private String book_author;
    private String book_publish;
    private String book_profile;
    private Integer book_appointment;
    private Integer book_borrow;


}
