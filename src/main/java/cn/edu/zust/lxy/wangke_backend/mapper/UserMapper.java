package cn.edu.zust.lxy.wangke_backend.mapper;

import cn.edu.zust.lxy.wangke_backend.model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

/**
 * @author keboom
 * @date 2021/6/14 12:27
 */
@Mapper
public interface UserMapper {

    @Insert("insert into user(user_name,user_password,user_phoneNum) values(#{user_name}, #{user_password}, #{user_phoneNum})")
    @Options(useGeneratedKeys = true, keyProperty = "user_id")
    boolean insert(User user);

    @Select("select user_name, user_password from user where user_name = #{username}")
    User findByName(String username);

    @Select("select user_id from user where user_name = #{username}")
    Integer getIdByName(String username);

    @Select("insert into appointment(book_id, user_id) values(#{bookId}, #{userId})")
    void appointment(int bookId, int userId);

    @Select("insert into borrow(book_id, user_id) values(#{bookId}, #{userId})")
    void borrow(int bookId, int userId);


}
