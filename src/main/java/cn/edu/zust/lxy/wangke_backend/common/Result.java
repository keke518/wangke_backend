package cn.edu.zust.lxy.wangke_backend.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author keboom
 * @date 2021/6/14 14:12
 */
@Setter
@Getter
@ToString
@AllArgsConstructor
public class Result<T> {

    private int code;
    private String message;
    private T data;
}
