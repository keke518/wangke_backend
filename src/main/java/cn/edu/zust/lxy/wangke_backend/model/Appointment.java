package cn.edu.zust.lxy.wangke_backend.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @author keboom
 * @date 2021/6/15 16:26
 */
@Setter
@Getter
@ToString
public class Appointment {

    private Integer appointment_id;
    private Integer book_id;
    private Integer user_id;


}
