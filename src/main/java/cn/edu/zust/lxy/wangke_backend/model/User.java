package cn.edu.zust.lxy.wangke_backend.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author keboom
 * @date 2021/6/14 12:21
 */
@Setter
@Getter
@ToString
public class User {

    private Integer user_id;
    private String user_name;
    private String user_password;
    private String user_phoneNum;
    private Integer user_creditScore;
}
