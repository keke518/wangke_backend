package cn.edu.zust.lxy.wangke_backend.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author keboom
 * @date 2021/6/15 16:27
 */
@Setter
@Getter
@ToString
public class Borrow {

    private Integer borrow_id;
    private Integer book_id;
    private Integer user_id;
}
