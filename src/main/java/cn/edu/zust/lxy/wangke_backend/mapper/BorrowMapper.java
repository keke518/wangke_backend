package cn.edu.zust.lxy.wangke_backend.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author keboom
 * @date 2021/6/15 16:29
 */
@Mapper
public interface BorrowMapper {

    @Select("select book_id from borrow where user_id = #{userId}")
    List<Integer> getIdByUserId(Integer userId);
}
